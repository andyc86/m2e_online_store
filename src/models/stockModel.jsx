import { observable } from "mobx"

export default class InventoryItem {
    @observable name;
    @observable category;
    @observable price;
    @observable totalStock;

    constructor(props) {
        this.defaultText = "";
        this.category = "";
        this.price = 0.00;
        this.totalStock = 0;
    }
}