import React from "react";
import { Provider } from "mobx-react";
import { render } from "react-dom";
import MuiThemeProvider from "material-ui/styles/MuiThemeProvider";
import TopNavBar from "./components/topNavBar"
import { BrowserRouter, browserHistory } from "react-router-dom";
import BasketDialog from "./components/sub-components/basketDialog";

import InventoryStore from "./stores/inventoryStore";
let store = new InventoryStore();

import Routes from "./routes";
import "./styles.styl"

render(
  <Provider inventoryStore={store}>
    <MuiThemeProvider>
      <TopNavBar></TopNavBar>
      <BrowserRouter history={browserHistory}>
        <div className={"page-content"}>
          {Routes}
        </div>
      </BrowserRouter>
      <BasketDialog></BasketDialog>
    </MuiThemeProvider>
  </Provider>,
  document.getElementById("root")
);
