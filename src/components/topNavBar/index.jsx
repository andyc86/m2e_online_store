import React, { Component } from "react";
import { observable, action } from "mobx";
import { observer, inject } from "mobx-react";

import { Route } from "react-router-dom";

import "./styles.styl";

@inject('inventoryStore')
@observer
export default class TopNavBar extends Component {
	constructor(props) {
        super(props);
	}
	render() {
        let { inventoryStore } = this.props;
		return (
            <div className={"site-top-nav"}>
                <div className={"site-logo"}>M2E</div>
                <div className={"nav-links"}>
                    <a href="/"><div className={`nav-link ${location.pathname==='/' ? 'active' : null}`}>Home</div></a>
                </div>
                <div className={"basket"} onClick={() => inventoryStore.toggleBasket(true)}>
                    <i className={"fa fa-shopping-basket"}></i>
                    <span>Basket</span>
                </div>
            </div>
		)
	}
}