import React, { Component } from "react";
import { observable, action } from "mobx";
import { observer, inject } from "mobx-react";
import RaisedButton from "material-ui/RaisedButton";

import "./stockItem.styl";

@inject("inventoryStore")
@observer
export default class StockItem extends Component {
    constructor(props) {
        super(props)
    }
    render() {
        let { item, inventoryStore, className } = this.props;        
        let originalItem = _.find(inventoryStore.stockList, { 'id': item.id });

        return (
            <div className={`stock-item ${(className) ? className : ''}`} style={{backgroundImage: `url(${require(`../../assets/img/${item.img}.png`)})`}}>
                <div className={"item-image"}><img src={require(`../../assets/img/${item.img}.png`)}/></div>
                <div className={"item-details"}>
                    <div className={"text"}>
                        <div className={"name"}>{item.name}</div>
                        <div className={"price"}>£{item.price}</div>
                    </div>
                    <div className={"controls"}>
                        <div className={"add-controls"}>
                            {(item.totalStock > 0) ?
                                <RaisedButton 
                                    className={"add-to-basket-btn"}
                                    onClick={() => inventoryStore.addItemToBasket(item)}>
                                    Add To Basket
                                </RaisedButton>
                                :
                                <div>Out of Stock</div>
                            }
                        </div>
                        <div className={"basket-controls"}>
                            <div className={"quantity-controls"}>
                                <div className={"decrement"} onClick={() => inventoryStore.removeItemFromBasket(originalItem)}>
                                    <span className="fa-stack">
                                        <i className="fa fa-square-o fa-stack-2x"></i>
                                        <i className={"fa fa-minus fa-stack-1x"}></i>
                                    </span>
                                </div>
                                <div className={"total"}>
                                    <span>{item.totalStock}</span>
                                </div>
                                <div className={`increment ${(originalItem.totalStock < 1) ? 'disabled' : ''}`} onClick={() => (originalItem.totalStock > 0) ? inventoryStore.addItemToBasket(originalItem) : null }>
                                    <span className="fa-stack">
                                        <i className="fa fa-square-o fa-stack-2x"></i>
                                        <i className={"fa fa-plus fa-stack-1x"}></i>
                                    </span>
                                </div>
                            </div>
                            <div className={"total-price"}>
                                <span>£{Math.round((item.price * item.totalStock)*100)/100}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}