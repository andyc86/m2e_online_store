import React, { Component } from "react";
import { observable, action } from "mobx";
import { observer, inject } from "mobx-react";
import StockItem from "./stockItem"
import RaisedButton from "material-ui/RaisedButton";

import "./basketDialog.styl";

@inject('inventoryStore')
@observer
export default class BasketDialog extends Component {
	constructor(props) {
        super(props);
    }
    handleKeyPress(e) {
        if (e.key != 'Enter') return;
        this.props.inventoryStore.validateDiscountCode(e.target.value);
    }
	render() {
        let { inventoryStore } = this.props;
        // Store property determines if dialog displays or not
        if (!inventoryStore.showBasket) return null;
        
        let { basket } = inventoryStore;
        let items = basket.items.map(item => { return <StockItem item={item} className={'basket'}></StockItem> });
        
        // Get the total value of the items added to the basket my multiplying
        // price and quantity, then subtract the discount value if applied.
        let basketTotal = _.sum(basket.items.map(i => i.price * i.totalStock))
        basketTotal = Math.round(((basket.discountAmount > 0 && basketTotal > 5) ? basketTotal - basket.discountAmount : basketTotal) * 100)/100
		return (
            <div className={"basket-dialog"}>
                <div class="modal-background" onClick={() => inventoryStore.toggleBasket()}></div>
                <div className={"modal"}>
                    <div className={"modal-header"}>My Basket</div>
                    <div className={"modal-content"}>
                        <div className={"item-list"}>
                            {items}
                        </div>
                        <div className={"basket-total"}>
                            <div className={"discount-code"}>
                                {(basket.discountAmount === 0) ? null : <i onClick={() => inventoryStore.removeDiscount() } className={"fa fa-times"}></i>}
                                <span>{`Discount ${(basket.discountAmount === 0) ? `Code` : `Applied`}:`} </span>
                                {(basket.discountAmount === 0) ? <input type="text" disabled={basketTotal === 0} onKeyPress={(e) => { this.handleKeyPress(e) } } /> : <span className={"discount-amount"}>-£{basket.discountAmount}</span>}
                            </div>
                            <div className={"amount"}>
                                Total: £{basketTotal}
                            </div>
                        </div>
                    </div>
                    <div className={"modal-footer"}>
                        <RaisedButton className={"continue"} onClick={() => inventoryStore.toggleBasket()}>Continue Shopping</RaisedButton>
                        <RaisedButton className={"checkout"} disabled>Proceed to Checkout</RaisedButton>
                    </div>
                </div>                    
            </div>
		)
	}
}