import React, { Component } from "react";
import { observable, action } from "mobx";
import { observer, Provider, inject } from "mobx-react";

import StockItem from "../sub-components/stockItem";

import "./styles.styl";

@inject('inventoryStore')
@observer
export default class HomePage extends Component {
	constructor(props) {
		super(props);
		let { inventoryStore } = props;
		inventoryStore.doInitialSet();
		inventoryStore.getInventory();
		inventoryStore.getBasket();
	}
	render() {
		let { inventoryStore } = this.props;

		// List all unique categories pulled from stock 'database'
		// Group items under each category to display on screen
		let categories = _.uniq(inventoryStore.stockList.map(s => { return s.category }));
		let groupedItems = categories.map(c => {
			return (
				<div className={"category"}>
					<div className={"category-title"}>{c}</div>
					<div className="stock-list">
						{_.filter(inventoryStore.stockList, { 'category': c }).map(si => { return <StockItem item={si}></StockItem> })}
					</div>
				</div>
			)
		})
		return (
			<div className={"home-page"}>
				{groupedItems}
			</div>
		)
	}
}