import React, { Component } from "react";
import { observable, action } from "mobx";
import { observer, Provider } from "mobx-react";

import "./styles.styl";

@observer
export default class NotFoundPage extends Component {
	constructor(props) {
		super(props);
	}
	render() {
		return (
			<div className={"not-found-page"}>
				Page Not Found!
			</div>
		)
	}
}