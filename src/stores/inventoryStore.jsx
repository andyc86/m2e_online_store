import React, { Component } from "react";
import { observable, computed, action } from "mobx";
import StockModel from "../models/stockModel";
import stockData from "./dummyData";
var _ = require('lodash');

export default class InventoryStore {
    @observable stockList = [];
    @observable basket = {
        discountAmount: 0,
        items: []
    };
    @observable showBasket = false;

    @action
    doInitialSet(value) {
        // Dev control - data reset switch.
        // Uncomment the following 2 lines and refresh page to reset localstorage data store.
        // Make sure to re-comment when running again!

        // localStorage.setItem('m2e_data', JSON.stringify({ "stock": stockData }));
        // localStorage.removeItem('m2e_basket');

        //Check if localstorage already exists, and if not, populate it with the dummy data array.
        const ls = JSON.parse(localStorage.getItem('m2e_data'));
		if (!ls || !ls.stock) {
			localStorage.setItem('m2e_data', JSON.stringify({ "stock": stockData }));
		}
    }

    // Simulating API call 'get all' stock to display
    @action
    getInventory(value) {
        let sl = JSON.parse(localStorage.getItem('m2e_data')).stock;        
        this.stockList = _.sortBy(sl, s => { return s.category; });
    }

    // Simulating API call to POST updates to stock database
    @action
    saveInventory() {
        return new Promise((resolve, reject) => {
            localStorage.setItem('m2e_data', JSON.stringify({ "stock": this.stockList }));
            resolve();
        });
    }

    // Show/Hide the basket dialog
    @action
    toggleBasket() {
        this.showBasket = !this.showBasket;
    }

    // Get all basket data preserved in localstorage, for returning users
    @action
    getBasket() {
        let ls_basket = JSON.parse(localStorage.getItem('m2e_basket'));
        if (ls_basket) {
            this.basket = ls_basket;
        }
    }

    // Save basket data in localstorage for potentially returning users
    @action
    saveBasket() {
        return new Promise((resolve, reject) => {
            localStorage.setItem('m2e_basket', JSON.stringify(this.basket));
            resolve();
        });
    }

    // Update a previously added item's quantity, or add a new item to the basket
    // Save after updating, then display the basket dialog (if not already displayed)
    @action
    addItemToBasket(item) {
        let itemInBasket = _.find(this.basket.items, { 'id': item.id });
        if (itemInBasket) {
            itemInBasket.totalStock++;
        } else {
            let newItem = Object.assign({}, item);
            this.basket.items.push(Object.assign(newItem, { totalStock: 1 }));
        }
        item.totalStock--;
        Promise.all([this.saveBasket(), this.saveInventory()]).then((res) => {
            this.showBasket = true;
        })
    }

    // Update a previously added item's quantity, or remove item if quantity was
    // at 1
    @action
    removeItemFromBasket(item){
        let itemInBasket = _.find(this.basket.items, { 'id': item.id });
        if (itemInBasket.totalStock > 1) {
            itemInBasket.totalStock--;
        } else {
            _.remove(this.basket.items, (i) => { return i.id == itemInBasket.id })
        }
        item.totalStock++;
        this.saveBasket()
        this.saveInventory()
    }

    // Super secure (and shameless) discount code validation.
    // Ideally this would be done in the API where a table of active codes could be queried.
    @action
    validateDiscountCode(code) {
        if (code.trim().toLowerCase() === 'hireme') {
            this.basket.discountAmount = 5;
            this.saveBasket();
        }
    }

    // Ability to remove a set discount code, should you wish to revert to default state
    @action
    removeDiscount() {
        this.basket.discountAmount = 0;
        this.saveBasket()
    }
};