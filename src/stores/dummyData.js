export default [
    {
        id: 1,
        name: "Leather Jacket",
        category: "Coats & Jackets",
        price: 54.99,
        totalStock: 6,
        img: "leather_jacket"
    },
    {
        id: 2,
        name: "Blue T-Shirt",
        category: "T-shirts",
        price: 8,
        totalStock: 11,
        img: "blue_tshirt"
    },
    {
        id: 3,
        name: "Black Jeans",
        category: "Jeans",
        price: 32.99,
        totalStock: 8,
        img: "black_jeans"
    },
    {
        id: 4,
        name: "Brown Shoes",
        category: "Shoes",
        price: 29.99,
        totalStock: 7,
        img: "brown_shoes"
    },
    {
        id: 5,
        name: "Blue Jeans",
        category: "Jeans",
        price: 32.99,
        totalStock: 2,
        img: "blue_jeans"
    },
    {
        id: 6,
        name: "Black Shoes",
        category: "Shoes",
        price: 25,
        totalStock: 10,
        img: "black_shoes"
    },
    {
        id: 7,
        name: "Trainers",
        category: "Shoes",
        price: 38.99,
        totalStock: 9,
        img: "trainers"
    },
    {
        id: 8,
        name: "Parka Jacket",
        category: "Coats & Jackets",
        price: 49.99,
        totalStock: 8,
        img: "parka_jacket"
    },
    {
        id: 9,
        name: "Bed Slippers",
        category: "Shoes",
        price: 9.99,
        totalStock: 20,
        img: "bed_slippers"
    },
    {
        id: 10,
        name: "Pink T-Shirt",
        category: "T-shirts",
        price: 8,
        totalStock: 5,
        img: "pink_tshirt"
    },
    {
        id: 11,
        name: "Yellow T-Shirt",
        category: "T-shirts",
        price: 8,
        totalStock: 6,
        img: "yellow_tshirt"
    },
    {
        id: 12,
        name: "Red T-Shirt",
        category: "T-shirts",
        price: 8,
        totalStock: 6,
        img: "red_tshirt"
    }
]