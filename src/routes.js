import React from "react";
import { Switch, Route } from "react-router-dom";

import HomePage from "./components/home";
import NotFoundPage from "./components/notFound"

export default (
    <Switch>
        <Route exact path="/" component={HomePage}/>
        <Route component={NotFoundPage}/>
    </Switch>
)