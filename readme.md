# M2E Front End Task

App built using Node.js version **10.11.0** and NPM version **6.4.1**

### Installation & Launch
In order to run the app, please use the following steps:
Open cmd in repository, then
```sh
$ npm install
``` 
Once this has completed
```sh
$ npm start
```
to run the app on **localhost:8080**

### Requirements
Use the code **HIREME** to test the £5 discount feature.